//
//  main.m
//  HeavyRotation
//
//  Created by Daniel Hirschlein on 8/30/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RotationAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RotationAppDelegate class]));
    }
}
