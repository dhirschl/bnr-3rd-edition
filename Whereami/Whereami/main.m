//
//  main.m
//  Whereami
//
//  Created by Daniel Hirschlein on 8/9/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WhereamiAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WhereamiAppDelegate class]));
    }
}
