//
//  BNRMapPoint.h
//  Whereami
//
//  Created by Daniel Hirschlein on 8/9/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface BNRMapPoint : NSObject <MKAnnotation>
{
    
}

// A new designated initializer for intances of BNRMapPoint
- (id) initWithCoordinate:(CLLocationCoordinate2D)c title:(NSString *)t;

// This is a required property for MKAnnotation
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

// This is an optional property from MKAnnotation
@property (nonatomic, copy) NSString *title;

@end