//
//  Line.m
//  TouchTracker
//
//  Created by Daniel Hirschlein on 10/11/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import "Line.h"

@implementation Line

@synthesize begin = _begin;
@synthesize end = _end;
@synthesize containingArray = _containingArray;

@end
