//
//  Line.h
//  TouchTracker
//
//  Created by Daniel Hirschlein on 10/11/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Line : NSObject

@property (nonatomic) CGPoint begin;
@property (nonatomic) CGPoint end;
@property (nonatomic, strong) NSMutableArray *containingArray;

@end
