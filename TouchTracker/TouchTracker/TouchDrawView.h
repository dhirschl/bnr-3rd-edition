//
//  TouchDrawView.h
//  TouchTracker
//
//  Created by Daniel Hirschlein on 10/11/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TouchDrawView : UIView
{
    NSMutableDictionary *linesInProcess;
    NSMutableArray *completeLines;
}

- (void)clearAll;
- (void)endTouches:(NSSet *)touches;
@end
