//
//  HypnosisterAppDelegate.h
//  Hypnosister
//
//  Created by Daniel Hirschlein on 8/14/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

// Don't forget this import statement!
#import "HypnosisView.h"

@interface HypnosisterAppDelegate : UIResponder <UIApplicationDelegate, UIScrollViewDelegate>
{
    HypnosisView *view;
}
@property (strong, nonatomic) UIWindow *window;

@end
