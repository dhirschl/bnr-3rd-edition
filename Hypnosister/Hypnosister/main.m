//
//  main.m
//  Hypnosister
//
//  Created by Daniel Hirschlein on 8/14/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HypnosisterAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HypnosisterAppDelegate class]));
    }
}
