//
//  HomepwnerAppDelegate.h
//  Homepwner
//
//  Created by Daniel Hirschlein on 8/30/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomepwnerAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
