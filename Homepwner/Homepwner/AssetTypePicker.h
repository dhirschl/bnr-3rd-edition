//
//  AssetTypePicker.h
//  Homepwner
//
//  Created by Daniel Hirschlein on 9/24/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BNRItem;

@interface AssetTypePicker : UITableViewController

@property (nonatomic, strong) BNRItem *item;

@end
