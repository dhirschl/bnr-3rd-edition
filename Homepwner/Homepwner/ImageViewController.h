//
//  ImageViewController.h
//  Homepwner
//
//  Created by Daniel Hirschlein on 9/13/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController
{
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIImageView *imageView;
}

@property (nonatomic, strong) UIImage *image;

@end
