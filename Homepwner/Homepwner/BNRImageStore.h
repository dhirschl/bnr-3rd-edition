//
//  BNRImageStore.h
//  Homepwner
//
//  Created by Daniel Hirschlein on 9/6/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNRImageStore : NSObject
{
    NSMutableDictionary *dictionary;
}
+ (BNRImageStore *)sharedStore;

- (void)setImage:(UIImage *)i forKey:(NSString *)s;
- (UIImage *)imageForKey:(NSString *)s;
- (void)deleteImageForKey:(NSString *)s;
- (NSString *)imagePathForKey:(NSString *)key;
@end
