//
//  HypnosisView.h
//  Hypnosister
//
//  Created by Daniel Hirschlein on 8/14/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HypnosisView : UIView
{
    
}
@property (nonatomic, strong) UIColor *circleColor;
@end
