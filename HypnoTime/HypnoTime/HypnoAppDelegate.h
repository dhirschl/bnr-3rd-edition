//
//  HypnoAppDelegate.h
//  HypnoTime
//
//  Created by Daniel Hirschlein on 8/16/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HypnoAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
