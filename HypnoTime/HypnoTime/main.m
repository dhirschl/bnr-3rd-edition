//
//  main.m
//  HypnoTime
//
//  Created by Daniel Hirschlein on 8/16/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HypnoAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HypnoAppDelegate class]));
    }
}
