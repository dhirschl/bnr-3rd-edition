//
//  QuizViewController.h
//  Quiz
//
//  Created by Daniel Hirschlein on 8/8/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizViewController : UIViewController
{
    int currentQuestionIndex;
    
    // The model objects
    NSMutableArray *questions;
    NSMutableArray *answers;
    
    // The view objects
    IBOutlet UILabel *questionField;
    IBOutlet UILabel *answerField;
    
}
- (IBAction)showQuestion:(UIButton *)sender;
- (IBAction)showAnswer:(UIButton *)sender;

@end
