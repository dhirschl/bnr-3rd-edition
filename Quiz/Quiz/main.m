//
//  main.m
//  Quiz
//
//  Created by Daniel Hirschlein on 8/8/12.
//  Copyright (c) 2012 Daniel Hirschlein. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QuizAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QuizAppDelegate class]));
    }
}
